#pragma once

#include "Cell.hpp"
#include "engine/GameObject.hpp"
#include "engine/Work.hpp"
#include "Field.hpp"
#include "Player.hpp"

#include <jngl/color.hpp>
#include <map>
#include <list>
#include <iostream>
#include <array>

class Game : public Work {
public:
	Game();
	~Game();
	void step() override;
	void draw() const override;

	void add(GameObject*);
	void remove(GameObject*);

	/// Gibt eine Referenz auf das Field zurück, damit z. B. Snake dieses ändern kann
	Field& getField();

	void onQuitEvent() override;

private:
	void addObjects();
	void removeObjects();

	std::vector<std::unique_ptr<GameObject>> gameObjects;

	/// Objekte die im nächsten Frame hinzugefügt werden sollen
	std::vector<std::unique_ptr<GameObject>> needToAdd;

	/// Objekte die im nächsten Frame entfernt werden sollen
	std::vector<const GameObject*> needToRemove;

	Field field;
	std::array<Player, 2> players;
};
