#include "../engine/Options.hpp"
#include "../engine/Paths.hpp"
#include "../init.hpp"

#include <ctime>
#include <jngl.hpp>

JNGL_MAIN_BEGIN {
	std::srand(std::time(0));
	if (init()) {
#ifdef NDEBUG
		try {
#endif
			jngl::setStepsPerSecond(60);
			jngl::setLineWidth(2);
			jngl::setIcon(GetPaths().data() + "gfx/icon.png");
			jngl::mainLoop();
			getOptions().Save();
#ifdef NDEBUG
		} catch (std::exception& e) {
			jngl::errorMessage(e.what());
		}
#endif
	}
} JNGL_MAIN_END
