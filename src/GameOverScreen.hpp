#pragma once

#include <jngl/work.hpp>
#include <jngl/text.hpp>

class GameOverScreen : public jngl::Work {
public:
	GameOverScreen(std::shared_ptr<Work> game, bool greenWon);
	void step() override;
	void draw() const override;
	void onQuitEvent() override;

private:
	std::shared_ptr<Work> game;
	double zoomIn = 0.0;
	jngl::Text text;
};
