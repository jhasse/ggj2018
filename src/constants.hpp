#pragma once

#include <string>
#include <jngl/color.hpp>

const std::string programShortName = "sushi";
const std::string programDisplayName = "Snake Line Transmission";

const jngl::Color colorPlayerA(0, 33, 182);
const jngl::Color colorPlayerB(43, 126, 26);

/// Die Größe einer Zelle im Spielfeld
const int CELL_SIZE = 16;

/// Die Breite und Größe des Spielfelds
const static int FIELD_SIZE = 62;
