#pragma once

#include <array>

class Vector2d;

bool pointInsideTriangle(const Vector2d& s, const std::array<Vector2d, 3>& triangle);
