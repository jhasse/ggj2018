#include "GameObject.hpp"

#include "../Game.hpp"

Vector2d GameObject::getPosition() const {
	return position;
}

void GameObject::addGameObject(GameObject* const gameObject) const {
	game_->add(gameObject);
}

void GameObject::SetGame(Game* game) {
	game_ = game;
}

Game& GameObject::GetGame() {
	return *game_;
}

Game* GameObject::game_ = nullptr;

double GameObject::getZIndex() const {
	if (zIndex) { return *zIndex; } else { return position.y; }
}

void GameObject::overwriteZIndex(const double zIndex) {
	this->zIndex = zIndex;
}

void MoveableObject::Accelerate(const Vector2d& acc) {
	acc_ += acc;
}

Vector2d MoveableObject::GetSpeed() const {
	return speed;
}

void MoveableObject::Move(const Vector2d& mov) {
	position += mov;
}

void MoveableObject::StepMovement() {
	speed += acc_;
	acc_.Set(0, 0);
	speed += Vector2d(0, 0.2);
	speed *= 0.95;
	position += speed;
}
