#include "Snake.hpp"

#include "Game.hpp"
#include "engine/Paths.hpp"

#include <jngl.hpp>

Snake::Snake(const FieldIndex pos, const FieldIndex direction, const Cell::Type cellType)
: fieldPos(pos - direction), direction(direction), initialDirection(direction), cellType(cellType) {
	// fieldPos wurde ein Feld "zurück" gesetzt, damit beim ersten aufruf von step das initiale Feld
	// gesetzt wird.
	jngl::play(GetPaths().data() + "sfx/snake.ogg");
}

void Snake::step() {
	if (--counter > 0) {
		return;
	}
	counter = 10; // Nur alle 10 Frames uns weiter setzen
	fieldPos += direction;
	if (rand() % 5 == 0) { // Mit einer Wahrscheinlichkeit von 0.2 die Richtung ändern
		const FieldIndex oldDirection = direction;
		if (rand() % 2 == 0) { // gegen den Uhrzeigersinn
			direction.i = -oldDirection.j;
			direction.j = oldDirection.i;
		} else { // im Uhrzeigersinn
			direction.i = oldDirection.j;
			direction.j = -oldDirection.i;
		}
		if (cellType == Cell::Type::NEUTRAL && direction.i == -initialDirection.i &&
		    direction.j == -initialDirection.j) {
			direction.i = -direction.i;
			direction.j = -direction.j;
		}
	}
	auto& field = GetGame().getField();
	auto& cell = field.getCell(fieldPos);
	if (cell.getType() == Cell::Type::NOTHING) {
		cell.setType(cellType);
	} else {
		GetGame().remove(this);
	}
}

void Snake::draw() const {
}
