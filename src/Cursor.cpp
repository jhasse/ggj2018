#include "Cursor.hpp"

#include "Game.hpp" // FIXME: Eigentlich sollten wir doch nur Field.hpp brauchen
#include "Snake.hpp"

Cursor::Cursor(const FieldIndex fieldPos, const FieldIndex direction, const int playerNr)
: fieldPos(fieldPos - direction), direction(direction), initialDirection(direction),
  playerNr(playerNr) {
	position = this->fieldPos.toScreenPos(); // Dies liegt außerhalb des Spielfelds
	targetPosition = fieldPos.toScreenPos(); // Und dies ist die richtige Position
}

void Cursor::step() {
	Vector2d animationDirection = (targetPosition - position);
	if (animationDirection.LengthSq() > 0) {
		animationDirection = animationDirection.Normalize();
	}
	const int FRAMES_PER_CELL = 4;
	position += animationDirection * (1.0 / FRAMES_PER_CELL) * CELL_SIZE;
	if (--counter > 0) {
		return;
	}
	counter = FRAMES_PER_CELL; // Nur alle FRAMES_PER_CELL Frames uns weiter setzen

	const auto& field = GetGame().getField();

	// Als erstes gucken, ob wir Richtung Ziel gehen können, das wäre ideal:
	if (field.getCell(fieldPos + initialDirection).isMine(playerNr)) {
		direction = initialDirection;
	} else {
		if (direction == initialDirection) { // ist direction nicht senkrecht zu initialDirection?
			if (rand() % 2 == 0) { // gegen den Uhrzeigersinn
				direction.i = -initialDirection.j;
				direction.j = initialDirection.i;
			} else { // im Uhrzeigersinn
				direction.i = initialDirection.j;
				direction.j = -initialDirection.i;
			}
		}
		if (!field.getCell(fieldPos + direction).isMine(playerNr)) {
			// Vielleicht können wir noch in die gegenüberliegende Richtung:
			if (field.getCell(fieldPos - direction).isMine(playerNr)) {
				direction.i = -direction.i;
				direction.j = -direction.j;
			} else {
				return; // Wir haben alles versucht, wir können uns nicht bewegen!
			}
		}
	}
	fieldPos += direction;

	targetPosition = fieldPos.toScreenPos();
}

void Cursor::draw() const {
	jngl::setColor(255, 255, 255);
	jngl::pushAlpha(200);
	jngl::drawEllipse(position.x, position.y, 8, 8);
	jngl::popAlpha();
}

std::pair<FieldIndex, FieldIndex> Cursor::getSpawnPos(boost::optional<int> playerNr) const {
	const auto& field = GetGame().getField();
	FieldIndex snakeDirection{ 0, 0 };

	FieldIndex directionsToTry[] = {
		initialDirection,                                       // nach "vorne"
		FieldIndex{ -initialDirection.j, initialDirection.i },  // nach links
		FieldIndex{ initialDirection.j, -initialDirection.i },  // nach rechts
		FieldIndex{ -initialDirection.i, -initialDirection.j }, // nach "hinten"
	};
	if (rand() % 2 == 0) { // Ob wir nach rechts oder links gehen soll zufällig sein
		std::swap(directionsToTry[1], directionsToTry[2]); // links rechts tauschen
	}
	for (const auto direction : directionsToTry) {
		const auto cellType = field.getCell(fieldPos + direction).getType();
		if (cellType == Cell::Type::NOTHING ||
		    (playerNr && ((*playerNr == 0 && cellType == Cell::Type::PLAYER_B) ||
		                  (*playerNr == 1 && cellType == Cell::Type::PLAYER_A)))) {
			snakeDirection = direction;
			break;
		}
	}
	return {fieldPos + snakeDirection, snakeDirection};
}
