#pragma once

#include <jngl/font.hpp>

namespace fonts {

std::shared_ptr<jngl::Font> veryBig();

};
