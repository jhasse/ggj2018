#pragma once

class Work;

class Widget {
public:
	Widget();
	virtual ~Widget();
	virtual void step() = 0;
	virtual void draw() const = 0;
	bool getSensitive() const;
	void setSensitive(bool);
	virtual void SetFocus(bool);
	virtual void OnFocusChanged();
	virtual void onAdd(Work&);
	void setDebug(bool);
	bool getDebug() const;
protected:
	bool sensitive_;
	bool focus_;
	bool debug;
};
