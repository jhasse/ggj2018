#pragma once

#include "engine/GameObject.hpp"
#include "FieldIndex.hpp"

#include <vector>

class Vector2d;
class Control;
class pull_type;
class Cursor;

/// Spieler 1 und 2
class Player : public virtual GameObject {
public:
	Player(size_t playerNr, std::unique_ptr<Control> = nullptr);
	Player(const Player&) = delete;
	Player(Player&&) = default;
	void step() override;
	void draw() const override;
	bool has_won();
	int getPlayerNr();
	bool isTransmitting() const;

protected:
	void spawnCursorOr(FieldIndex fieldPos, FieldIndex direction, std::function<void()>);

	std::unique_ptr<Control> control;

	/// Fortschritt der Übertragung in %. Bei 100 hat man gewonnen
	double progress = 0;

	const int playerNr;
	double speed;
	int p0rotation;
	int p1rotation;
	int cooldown_counter = 0;
	double waveStart = 0;

	/// Anzahl der Schüsse
	double ammo = 3.0;
	const static int MAX_AMMO = 3;

	/// Falls gerade ein Cursor durch unsere Schlangen wandert, zeigt dieser Zeiger auf ihn. Es gibt
	/// zwei, da wir für jede Seite einen haben wollen.
	Cursor* activeCursor[2] = { nullptr, nullptr };

	bool transmitting = true;
};
